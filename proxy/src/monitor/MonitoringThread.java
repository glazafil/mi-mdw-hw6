package monitor;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class MonitoringThread extends Thread {
	   private Thread t;
	   private String threadName;
	   private int period;
	   
	   private ArrayList<String> services;
	   private List<String> health;
	   
	   MonitoringThread( String name, int period) {
	      threadName = name;
	      System.out.println("Creating " +  threadName );
	      services = new ArrayList<String>();
	      services.add("http://147.32.233.18:8888/MI-MDW-LastMinute1/list");
	      services.add("http://147.32.233.18:8888/MI-MDW-LastMinute2/list");
	      services.add("http://147.32.233.18:8888/MI-MDW-LastMinute3/list");
	      this.period = period;
	   }
	   
	   public void run() {
	      System.out.println("Running " +  threadName );
	      health = Collections.synchronizedList(new ArrayList<String>());
	      try {
	         while(true) {
	        	 System.out.println("\nHealth services by " + this.threadName + ":");
	            for(String service : services) {
	            	 String url = service;
	            	 HttpURLConnection connection = (HttpURLConnection) (new URL(url)).openConnection();
	            	 connection.setRequestMethod("GET");
	            	 int code = connection.getResponseCode();
	            	 if(code == 200) {
	            		 health.add(service);
	            		 System.out.println(service);
	            	 }
	            }
	           sleep(period); 
	         }
	      }catch (InterruptedException | IOException e) {
	         System.out.println("Thread " +  threadName + " interrupted.");
	      }
	      
	      System.out.println("Thread " +  threadName + " exiting.");
	   }
	   
	   public void start () {
	      System.out.println("Starting " +  threadName );
	      if (t == null) {
	         t = new Thread (this, threadName);
	         t.start ();
	      }
	   }
	   
	   List<String> getServices() {
		   return health;
	   }
	}
